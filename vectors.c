#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 110000
#define TEST_SIZE 20

void ascending_ordered_vector(int *v, int size) {
	int i;
	for (i = 0; i < size; i++) {
		v[i] = i;
	}
}

void descending_ordered_vector(int *v, int size) {
   int i;
   for (i = 0; i < size; i++) {
	   v[i] = size-i-1;
   }
}

void random_vector(int *v, int size) {
	int i;
	srand(time(NULL));
	for (i = 0; i < size; i++) {
		v[i] = rand() % size;
	}
}

double partially_random_vector(int *v, int size) {
	int i;
	int rand_beg, rand_end;
	srand(time(NULL));
	rand_beg = rand() % size;
	rand_end = (rand() % (size-rand_beg-1)) + rand_beg;
	for (i = rand_beg; i < rand_end; i++) {
		v[i] = rand() % size;
	}
	
	for (i = rand_end; i < size; i++) {
		v[i] = i;
	}
	
	if (rand_beg > 0) {
		for (i = 0; i < rand_beg; i++) {
			v[i] = i;
		}
	}
	// returns the size of the random vector relative to the size of the total vector	
	return ((double) (rand_end - rand_beg))/size; 
}

void print_vector(int *v, int size) {
	int i;
	for (i = 0; i < size; i++) {
		printf("%d ", v[i]);
	}
	printf("\n");
}


int main() {
	int *v;
	double random_percentage;
	v = malloc(2 * TEST_SIZE * sizeof(int));
	
	// change function to test	
	random_percentage = partially_random_vector(v, TEST_SIZE) * 100;
	printf("Vector %.1lf % randomized.\n", random_percentage);
	print_vector(v, TEST_SIZE);
	free(v);
}
